package com.boot.jpa.jpo;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaEmpPjt;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Getter
@Setter
@Entity(name = "InsaEmpPjt")
@IdClass(InsaEmpPjtJpoId.class)
@Table(name = "INSA_EMP_PJT", schema = "INSA")
public class InsaEmpPjtJpo {

    @Id
    @Column(name = "RECT_REG_NO")
    private String rectRegNo;
    @Id
    @Column(name = "SABUN")
    private String sabun;
    @Column(name = "PUT_START_DAY")
    private Timestamp putStartDay;
    @Column(name = "PUT_END_DAY")
    private Timestamp putEndDay;
    @Column(name = "PROG_STATE_CODE")
    private String progStateCode;
    @Column(name = "REG_DAY")
    private String regDay;

    public InsaEmpPjtJpo() {
    }

    public InsaEmpPjtJpo(InsaEmpPjt entity) {
        BeanUtils.copyProperties(entity, this);
    }

    public InsaEmpPjt toDomain() {
        InsaEmpPjt retVal = new InsaEmpPjt();
        BeanUtils.copyProperties(this, retVal);
        return retVal;
    }

    public static List<InsaEmpPjt> toDomains(Iterable<InsaEmpPjtJpo> jpos) {
        return StreamSupport.stream(jpos.spliterator(), false).map((InsaEmpPjtJpo::toDomain)).collect(Collectors.toList());
    }

}
