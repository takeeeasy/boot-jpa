package com.boot.jpa.jpo;

import com.boot.jpa.domain.Insa;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Getter
@Setter
@Entity(name = "Insa")
@Table(name = "INSA", schema = "INSA")
public class InsaJpo {

    @Id
    @Column(name = "SABUN")
    private String sabun;

    @Column(name = "JOIN_DAY")
    private String joinDay;

    @Column(name = "RETIRE_DAY")
    private String retireDay;

    @Column(name = "PUT_YN")
    private String putYn;

    @Column(name = "CLASS_GBN_CODE")
    private String classGbnCode;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REG_NO")
    private String regNo;

    @Column(name = "ENG_NAME")
    private String engName;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "HP")
    private String hp;

    @Column(name = "CARRIER")
    private String carrier;

    @Column(name = "POS_GBN_CODE")
    private String posGbnCode;

    @Column(name = "CMP_REG_NO")
    private String cmpRegNo;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "YEARS")
    private BigDecimal years;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ZIP")
    private String zip;

    @Column(name = "ADDR1")
    private String addr1;

    @Column(name = "ADDR2")
    private String addr2;

    @Column(name = "MIL_YN")
    private String milYn;

    @Column(name = "HOME_PHONE")
    private String homePhone;

    @Column(name = "JOIN_GBN_CODE")
    private String joinGbnCode;

    @Column(name = "SALARY")
    private BigDecimal salary;

    @Column(name = "KOSA_REG_YN")
    private String kosaRegYn;

    @Column(name = "KOSA_CLASS")
    private String kosaClass;

    @Column(name = "PW")
    private String pw;

    public InsaJpo() {
    }

    public InsaJpo(Insa entity) {
        BeanUtils.copyProperties(entity, this);
    }

    public Insa toDomain() {
        Insa retVal = new Insa();
        BeanUtils.copyProperties(this, retVal);
        return retVal;
    }

    public static List<Insa> toDomains(Iterable<InsaJpo> jpos) {
        return StreamSupport.stream(jpos.spliterator(), false).map((InsaJpo::toDomain)).collect(Collectors.toList());
    }

}
