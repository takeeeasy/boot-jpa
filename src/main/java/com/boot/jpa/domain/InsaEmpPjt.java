package com.boot.jpa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@NoArgsConstructor
@Data
public class InsaEmpPjt {

    private String rectRegNo;
    private String sabun;
    private Timestamp putStartDay;
    private Timestamp putEndDay;
    private String progStateCode;
    private String regDay;
}
