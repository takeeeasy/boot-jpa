package com.boot.jpa.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
public class Insa {

    private String sabun;
    private String joinDay;
    private String retireDay;
    private String putYn;
    private String classGbnCode;
    private String name;
    private String regNo;
    private String engName;
    private String phone;
    private String hp;
    private String carrier;
    private String posGbnCode;
    private String cmpRegNo;
    private String sex;
    private BigDecimal years;
    private String email;
    private String zip;
    private String addr1;
    private String addr2;
    private String milYn;
    private String homePhone;
    private String joinGbnCode;
    private BigDecimal salary;
    private String kosaRegYn;
    private String kosaClass;
    private String pw;
}
