package com.boot.jpa.logic.crud;

import com.boot.jpa.domain.Insa;
import org.springframework.stereotype.Service;

import java.util.List;

public interface InsaService {
    //
    String register(Insa insa);
    List<Insa> findAll();
    Insa find(String id);

    void modify(Insa insa);
    void delete(String id);
}
