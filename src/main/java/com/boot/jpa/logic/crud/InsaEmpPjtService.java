package com.boot.jpa.logic.crud;

import com.boot.jpa.domain.InsaEmpPjt;

import java.util.List;

public interface InsaEmpPjtService {
    //
    String register(InsaEmpPjt entity);
    List<InsaEmpPjt> findAll();
    InsaEmpPjt find(String rectRegNo, String sabun);

    void modify(InsaEmpPjt entity);
    void delete(String rectRegNo, String sabun);
}
