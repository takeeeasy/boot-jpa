package com.boot.jpa.logic.crud;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.store.InsaStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsaLogic implements InsaService {
    //
    @Autowired
    private final InsaStore insaStore;

    public InsaLogic(InsaStore insaStore) {
        this.insaStore = insaStore;
    }

    @Override
    public String register(Insa insa) {
        if (insaStore.existsById(insa.getSabun())) {
            throw new RuntimeException(String.format("Employee(%s) already exists", insa.getSabun()));
        }

        this.insaStore.create(insa);
        return insa.getSabun();
    }

    @Override
    public List<Insa> findAll() {
        return this.insaStore.retrieveAll();
    }

    @Override
    public Insa find(String id) {
        return this.insaStore.retrieveById(id);
    }

    @Override
    public void modify(Insa insa) {
         insaStore.update(insa);
    }

    @Override
    public void delete(String id) {
        insaStore.delete(id);
    }
}
