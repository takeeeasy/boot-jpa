package com.boot.jpa.logic.crud;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaEmpPjt;
import com.boot.jpa.store.InsaEmpPjtStore;
import com.boot.jpa.store.InsaStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsaEmpPjtLogic implements InsaEmpPjtService {
    //
    @Autowired
    private final InsaEmpPjtStore insaEmpPjtStore;

    public InsaEmpPjtLogic(InsaEmpPjtStore insaEmpPjtStore) {
        this.insaEmpPjtStore = insaEmpPjtStore;
    }

    @Override
    public String register(InsaEmpPjt entity) {
        if (insaEmpPjtStore.existsById(entity.getRectRegNo(), entity.getSabun())) {
            throw new RuntimeException(String.format("Employee(%s, %s) already exists", entity.getRectRegNo() ,entity.getSabun()));
        }

        this.insaEmpPjtStore.create(entity);
        return entity.getRectRegNo()+", "+ entity.getSabun();
    }

    @Override
    public List<InsaEmpPjt> findAll() {
        return insaEmpPjtStore.retrieveAll();
    }

    @Override
    public InsaEmpPjt find(String rectRegNo, String sabun) {
        return insaEmpPjtStore.retrieveById(rectRegNo, sabun);
    }

    @Override
    public void modify(InsaEmpPjt entity) {
        this.insaEmpPjtStore.update(entity);
    }

    @Override
    public void delete(String rectRegNo, String sabun) {
        this.insaEmpPjtStore.delete(rectRegNo, sabun);
    }
}
