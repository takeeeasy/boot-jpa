package com.boot.jpa.logic.aggregate;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaMergeDto;
import com.boot.jpa.logic.crud.InsaEmpPjtService;
import com.boot.jpa.logic.crud.InsaService;
import com.google.gson.Gson;
import net.minidev.json.JSONUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class InsaAggrLogic implements InsaAggrService {

    private InsaService insaService;
    private InsaEmpPjtService insaEmpPjtService;

    public InsaAggrLogic() {
    }

    @Override
    public void setInsaServiec(InsaService insaService) {
        this.insaService = insaService;
    }

    @Override
    public void setInsaEmpPjtService(InsaEmpPjtService insaEmpPjtService) {
        this.insaEmpPjtService = insaEmpPjtService;
    }

    @Override
    public List<Insa> findAllByName(String name) {
//        Supplier<String> compare = () -> StringUtils.isEmpty(entity.getName()) ? "" : entity.getName() ;
        return this.insaService.findAll()
                .stream()
                .filter(dto -> dto.getName().equals(((Supplier<String>) () ->
                        StringUtils.isEmpty(name) ? "" : name).get()))
                .collect(Collectors.toList());
    }

    @Override
    public List<InsaMergeDto> joiningInsaEmpPjt(InsaMergeDto entity) {
        List<InsaMergeDto> list = this.insaEmpPjtService.findAll().stream().map(insaEmpPjt -> {
            InsaMergeDto dto = new InsaMergeDto();
            BeanUtils.copyProperties(insaEmpPjt, dto);
            return dto;
        }).collect(Collectors.toList());
        System.out.println(new Gson().toJson(list));

//        this.findAllByName(entity.getName());
//        List<Insa> insa = this.insaService.findAll();
//        list.stream().filter(dto -> dto.getSabun() == insa.stream()
//                .filter(dto2 -> dto2.getSabun() == dto.getSabun()).map(dto3 -> dto3.getSabun())
//                .reduce()).collect(Collectors.toList());

        return list;
    }
}
