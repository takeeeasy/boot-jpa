package com.boot.jpa.logic.aggregate;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaMergeDto;
import com.boot.jpa.logic.crud.InsaEmpPjtService;
import com.boot.jpa.logic.crud.InsaService;

import java.util.List;

public interface InsaAggrService {

    void setInsaServiec(InsaService insaService);
    void setInsaEmpPjtService(InsaEmpPjtService insaEmpPjtService);

    List<Insa> findAllByName(String name);
    List<InsaMergeDto> joiningInsaEmpPjt(InsaMergeDto entity);
}
