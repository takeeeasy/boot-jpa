package com.boot.jpa.rest;

import com.boot.jpa.bind.ServiceLifeCycle;
import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaMergeDto;
import com.boot.jpa.logic.aggregate.InsaAggrService;
import com.boot.jpa.logic.crud.InsaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping( path = "insa" )
public class InsaResource {

    private final InsaService insaService;
    private final InsaAggrService insaAggrService;

    public InsaResource(ServiceLifeCycle serviceLifeCycle) {
        this.insaService = serviceLifeCycle.requestInsaService();
        this.insaAggrService = serviceLifeCycle.requestInsaAggrService();

    }

    @PostMapping(path= {"/sabun"})
    public String register(@RequestBody Insa insa){ return this.insaService.register(insa); }

    @PutMapping(path= {"/sabun"})
    public void modify(@RequestBody Insa insa){ this.insaService.modify(insa); }

    @GetMapping(path="/sabun/{sabun}")
    public Insa find(@PathVariable("sabun") String sabun) {
        return insaService.find(sabun);
    }

    @GetMapping(path= {"","/"})
    public List<Insa> findAll(){ return this.insaService.findAll(); }

    @PostMapping(path = "/findByName")
    public List<Insa> findAllByName(@RequestBody Insa entity) { return this.insaAggrService.findAllByName(entity.getName()); }

    @PostMapping(path = "/mergeStream")
    public List<InsaMergeDto> joiningInsaEmpPjt(@RequestBody InsaMergeDto entity) { return this.insaAggrService.joiningInsaEmpPjt(entity); }

}
