package com.boot.jpa.store;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.jpo.InsaJpo;
import com.boot.jpa.repository.InsaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
@Repository
public class InsaJpaStore implements InsaStore {
    //
    private final InsaRepository insaRepository;

    @Autowired
    public InsaJpaStore(InsaRepository insaRepository) {
        //
        this.insaRepository = insaRepository;
    }

    @Override
    public void create(Insa insa) {
        //
        this.insaRepository.save(new InsaJpo(insa));
    }

    @Override
    public List<Insa> retrieveAll() {
        //
        List<InsaJpo> insaJpos = this.insaRepository.findAll();
        return insaJpos.stream()
                .map(InsaJpo::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Insa retrieveById(String id) {
        //
        Optional<InsaJpo> insaJpo = this.insaRepository.findById(id);
        if (!insaJpo.isPresent()) {
            throw new NoSuchElementException(String.format("employee(%s) is not found.", id));
        }

        return insaJpo.get().toDomain();
    }

    @Override
    public boolean existsById(String id) {
        //
        return insaRepository.existsById(id);
    }

    @Override
    public void update(Insa insa) {
        //
        this.insaRepository.save(new InsaJpo(insa));
    }

    @Override
    public void delete(String id) {
        this.insaRepository.deleteById(id);
    }
}
