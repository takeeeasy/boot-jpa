package com.boot.jpa.store;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaEmpPjt;

import java.util.List;

public interface InsaEmpPjtStore {
    //
    void create(InsaEmpPjt entity);
    List<InsaEmpPjt> retrieveAll();
    InsaEmpPjt retrieveById(String rectRegNo, String sabun);
    boolean existsById(String rectRegNo, String sabun);
    void update(InsaEmpPjt entity);
    void delete(String rectRegNo, String sabun);
}
