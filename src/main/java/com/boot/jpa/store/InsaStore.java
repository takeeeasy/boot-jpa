package com.boot.jpa.store;

import com.boot.jpa.domain.Insa;

import java.util.List;

public interface InsaStore {
    //
    void create(Insa insa);
    List<Insa> retrieveAll();
    Insa retrieveById(String id);
    boolean existsById(String id);
    void update(Insa insa);
    void delete(String id);
}
