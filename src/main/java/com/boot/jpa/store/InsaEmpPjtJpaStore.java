package com.boot.jpa.store;

import com.boot.jpa.domain.Insa;
import com.boot.jpa.domain.InsaEmpPjt;
import com.boot.jpa.jpo.InsaEmpPjtJpo;
import com.boot.jpa.jpo.InsaJpo;
import com.boot.jpa.repository.InsaEmpPjtRepository;
import com.boot.jpa.repository.InsaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InsaEmpPjtJpaStore implements InsaEmpPjtStore {
    //
    private final InsaEmpPjtRepository insaEmpPjtRepository;

    @Autowired
    public InsaEmpPjtJpaStore(InsaEmpPjtRepository insaEmpPjtRepository) {
        //
        this.insaEmpPjtRepository = insaEmpPjtRepository;
    }

    @Override
    public void create(InsaEmpPjt entity) {
        //
        this.insaEmpPjtRepository.save(new InsaEmpPjtJpo(entity));
    }

    @Override
    public List<InsaEmpPjt> retrieveAll() {
        //
        List<InsaEmpPjtJpo> insaJpos = this.insaEmpPjtRepository.findAll();
        return insaJpos.stream()
                .map(InsaEmpPjtJpo::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public InsaEmpPjt retrieveById(String rectRegNo, String sabun) {
        //
        Optional<InsaEmpPjtJpo> insaJpo = this.insaEmpPjtRepository.findByRectRegNoAndSabun(rectRegNo, sabun);
        if (!insaJpo.isPresent()) {
            throw new NoSuchElementException(String.format("employee(%s, %s) is not found.", rectRegNo, sabun));
        }

        return insaJpo.get().toDomain();
    }

    @Override
    public boolean existsById(String rectRegNo, String sabun) {
        //
        return insaEmpPjtRepository.existsByRectRegNoAndSabun(rectRegNo, sabun);
    }

    @Override
    public void update(InsaEmpPjt entity) {
        //
        this.insaEmpPjtRepository.save(new InsaEmpPjtJpo(entity));
    }

    @Override
    public void delete(String rectRegNo, String sabun) {
        this.insaEmpPjtRepository.deleteByRectRegNoAndSabun(rectRegNo, sabun);
    }
}
