package com.boot.jpa.repository;

import com.boot.jpa.jpo.InsaJpo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsaRepository extends JpaRepository<InsaJpo, String> {
    boolean existsById(String id);

}
