package com.boot.jpa.repository;

import com.boot.jpa.jpo.InsaEmpPjtJpo;
import com.boot.jpa.jpo.InsaEmpPjtJpoId;
import com.boot.jpa.jpo.InsaJpo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InsaEmpPjtRepository extends JpaRepository<InsaEmpPjtJpo, InsaEmpPjtJpoId> {
    boolean existsByRectRegNoAndSabun(String rectRegNo, String sabun);

    Optional<InsaEmpPjtJpo> findByRectRegNoAndSabun(String rectRegNo, String sabun);

    void deleteByRectRegNoAndSabun(String rectRegNo, String sabun);
}
