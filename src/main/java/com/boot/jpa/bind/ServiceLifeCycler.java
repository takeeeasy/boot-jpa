package com.boot.jpa.bind;

import com.boot.jpa.logic.aggregate.InsaAggrService;
import com.boot.jpa.logic.crud.InsaEmpPjtService;
import com.boot.jpa.logic.crud.InsaService;
import org.springframework.stereotype.Component;

@Component
public class ServiceLifeCycler implements ServiceLifeCycle {
    /** L1 Logic */
    private final InsaService insaService;
    private final InsaEmpPjtService insaEmpPjtService;

    /** L2 Logic */
    private final InsaAggrService insaAggrService;

    public ServiceLifeCycler(InsaService insaService, InsaEmpPjtService insaEmpPjtService, InsaAggrService insaAggrService) {
        /** L1 Logic */
        this.insaService = insaService;
        this.insaEmpPjtService = insaEmpPjtService;

        /** L2 Logic */
        this.insaAggrService = insaAggrService;
        this.insaAggrService.setInsaServiec(insaService);
        this.insaAggrService.setInsaEmpPjtService(insaEmpPjtService);
    }

    @Override
    public InsaService requestInsaService() {
        return this.insaService;
    }

    @Override
    public InsaEmpPjtService requestInsaEmpPjtService() {
        return this.insaEmpPjtService;
    }

    @Override
    public InsaAggrService requestInsaAggrService() {
        return this.insaAggrService;
    }
}
