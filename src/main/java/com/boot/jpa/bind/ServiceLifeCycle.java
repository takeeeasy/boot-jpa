package com.boot.jpa.bind;

import com.boot.jpa.logic.aggregate.InsaAggrService;
import com.boot.jpa.logic.crud.InsaEmpPjtService;
import com.boot.jpa.logic.crud.InsaService;

public interface ServiceLifeCycle {

    InsaService requestInsaService();
    InsaEmpPjtService requestInsaEmpPjtService();
    InsaAggrService requestInsaAggrService();
}
