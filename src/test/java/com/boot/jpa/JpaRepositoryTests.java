package com.boot.jpa;

import com.boot.jpa.boot.JpaApplication;
import com.boot.jpa.repository.InsaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = JpaApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class JpaRepositoryTests {

    @Autowired
    InsaRepository insaRepository;

    @Test
    public void test() {
        System.out.println(insaRepository.findById("2012120101").get().toDomain().getName());
//        assert insaRepository != null;
    }
}
