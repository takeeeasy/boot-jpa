package com.boot.jpa;

import com.boot.jpa.boot.JpaApplication;
import com.boot.jpa.domain.Insa;
import com.boot.jpa.rest.InsaResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {JpaApplication.class})
public class JpaApplicationTests {

    @Autowired
    InsaResource insaResource;

    @Test
    public void contextLoads() {
        Insa entity = insaResource.find("2012120101");
        System.out.println(entity.getName());
//        assert entity.getSabun().equals("2012120101");
    }

}
